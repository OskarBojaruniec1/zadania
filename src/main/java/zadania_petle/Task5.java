package zadania_petle;

import java.util.Scanner;

public class Task5 {

    public static void main(String[] args) {

        // Declaration of password
        String password = "Polska";

        System.out.println("Prosze podać hasło: ");

        // User's input
        Scanner sc = new Scanner(System.in);
        String userInputPassword = sc.nextLine();

        // Loop which makes it able to repeat user's input if it's not equal to declared password
        while(!userInputPassword.equals(password)){

            System.out.println("Podaj poprawne hasło: ");
            userInputPassword = sc.nextLine();

        }

        System.out.println("Odgadłeś hasło!");

    }
}

package zadania_petle;

public class Task4 {

    public static void main(String[] args) {

        // Declaration of minimal and maximum number
        int min = 1;
        int max = 49;

        // Loop which is generating 6 random numbers
        for (int i = 1; i <= 6; i++) {

            int randomNumber = (int)Math.floor(Math.random()*(max-min+1)+min);
            System.out.println(randomNumber);

        }
    }
}

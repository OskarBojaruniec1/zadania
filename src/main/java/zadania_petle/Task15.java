package zadania_petle;

import java.util.Scanner;

public class Task15 {

    public static void main(String[] args) {

        // User input - a
        System.out.println("Podaj liczbę a: ");
        Scanner scA = new Scanner(System.in);
        int a = scA.nextInt();
        // User input - b
        System.out.println("Podaj liczbę b: ");
        Scanner scB = new Scanner(System.in);
        int b = scB.nextInt();

        // c - modulo of division a,b
        int c;

        while(a % b != 0){

            c = a % b;

            if(c == 0){
                break;
            }

            a = b;
            b = c;
        }

        System.out.println("NWD(a,b) = " + b);

    }
}

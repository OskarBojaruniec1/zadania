package zadania_petle;

import java.util.Scanner;

public class Task2 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Prosze podaj liczbę: ");

        // User's input
        int number = sc.nextInt();

        // sum - variable which will be sum of numbers from 1 to the user's number
        int sum = 1;

        // Loop that sums the numbers from 1 to the user's number
        for (int i = 2; i <= number; i++) {

            sum = sum + i;

        }

        System.out.println(sum);
    }
}

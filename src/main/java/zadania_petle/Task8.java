package zadania_petle;

import java.util.Scanner;

public class Task8 {

    public static void main(String[] args) {

        System.out.println("Proszę podaj wyraz: ");

        // User's input
        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine();

        // Convert user's word to chars
        char[] arr = word.toCharArray();

        // Iteration through every index of user's word to find numbers in it
        for (int i = 0; i < arr.length; i++) {

            if(Character.isDigit(word.charAt(i))){

                // Prints all numbers which are in user's word
                System.out.print(arr[i]);

            }
        }
    }
}



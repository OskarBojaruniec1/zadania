package zadania_petle;

import java.util.Scanner;

public class Task11 {

    public static void main(String[] args) {

        System.out.println("Podaj proszę liczbę: ");

        // User's input
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();

        /* Variables (divider - the number by which we will divide user's number (from his number to 1)),
        (counter - how many times modulo of user's number = 0)*/
        int divider = number;
        int counter = 0;

        // Loop which is dividing number of user and decrement variable "divider"
        int i = 0;
        while(i < number){

            System.out.println(divider);
            if(number % divider == 0){
                counter++;
            }
            i++;
            divider--;

        }

        if(counter == 2){
            System.out.println("Liczba pierwsza");
        }else if(counter > 2){
            System.out.println("Liczba złożona");
        }else if(number < 2){
            System.out.println("Twoja liczba nie jest liczbą pierwszą ani złożoną");
        }
    }
}

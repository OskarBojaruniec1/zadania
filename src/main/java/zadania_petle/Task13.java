package zadania_petle;

import java.util.Scanner;

public class Task13 {

    public static void main(String[] args) {

        System.out.println("Podaj proszę jakiś napis, sprawdzimy ile razy występuje w nim mała litera: ");

        // User's input
        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine();

        // Variable which is keeping how many letters are in lowerCase increments when letter is in lowerCase
        int counter = 0;

        // Convert word to chars
        char[] ch = word.toCharArray();

        // Loop which checks how many letters are in lowerCase
        for (char c : ch) {

            if (Character.isLowerCase(c)) {
                counter++;
            }

        }
        System.out.println("Małe litery występują " + counter + " razy!");

    }
}

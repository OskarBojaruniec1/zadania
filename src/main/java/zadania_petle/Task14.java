package zadania_petle;

import java.util.Scanner;

public class Task14 {

    public static void main(String[] args) {

        System.out.println("Podaj proszę imię");

        // User input
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();

        // Convert name to chars
        char[] nameChars = name.toCharArray();

        for (int i = 1; i <= nameChars.length - 1;) {

            System.out.print(nameChars[i]);
            i = i + 2;

        }
    }
}

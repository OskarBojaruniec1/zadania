package zadania_petle;

import java.util.Scanner;

public class Task3 {

    public static void main(String[] args) {

        // Number which user have to find
        int number = 600;

        // User's input
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj proszę liczbę: \n");
        int userNumber = sc.nextInt();

        // Loop which repeat as long as user will find number
        while(userNumber != number){

            if(userNumber < number){

                System.out.println("\nSzukana liczba jest większa");

            }else {

                System.out.println("\nSzukana liczba jest mniejsza");

            }
            System.out.println("Podaj proszę liczbę: \n");
            userNumber = sc.nextInt();

        }

        System.out.println("\nDokładnie chodziło o tę liczbę " + number);
    }
}

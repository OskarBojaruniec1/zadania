package zadania_petle;

import java.util.Scanner;

public class Task7 {

    public static void main(String[] args) {

        int answer;
        String name;
        String star = "";

        // Loop which is checking user's input and blocks other inputs
        while(true){

            Scanner userAnswer = new Scanner(System.in);

            System.out.println("Co byś chciał utowrzyć?");
            System.out.println("1 - Wieżyczka \n2 - Choinka");

            answer = userAnswer.nextInt();

            if(answer == 1){

                name = "Wieżyczka";
                break;

            }else if(answer == 2){

                name = "Choinka";
                break;
            }
        }

        // Declaration of size
        System.out.println("Jak duża ma być " + name + ":" );
        Scanner userSize = new Scanner(System.in);
        int size = userSize.nextInt();

        // Prints tower/christmas tree
        if(answer == 1){

            for (int i = 0; i < size; i++) {

                // Adds * at the end of String
                star = star.concat("*");
                System.out.println(star);

            }

        }else {

            for(int i = 1;i <= size;i++){

                for(int j = size-i;j > 0;j--)
                {
                    System.out.print(" ");
                }
                for(int k = 1;k <= i;k++)
                {
                    System.out.print("* ");
                }

                System.out.println();

            }
        }
    }
}


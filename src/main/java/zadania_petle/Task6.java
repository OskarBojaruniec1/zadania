package zadania_petle;

import java.util.Scanner;

public class Task6 {

    public static void main(String[] args) {

        Scanner scNumber = new Scanner(System.in);
        System.out.println("Obliczanie silni");
        System.out.println("Prosze podaj liczbę: ");

        // User's input
        int number = scNumber.nextInt();

        // Info for user about maximum number which he can declare
        while(number > 20){

            System.out.println("Niestey wynik dla twojej liczby wyjdzie niepoprawny (jest większa niż 20).");

            // User's Answer
            Scanner scAnswer = new Scanner(System.in);
            System.out.println("1 - Kontynuuj \n2 - Zmień liczbę");
            int Answer = scAnswer.nextInt();

            // Statement which check what is user's answer
            if(Answer == 1){

                break;

            }else if(Answer == 2){

                System.out.println("Prosze podaj liczbę: ");
                number = scNumber.nextInt();

            }

        }

        // factorial - variable which value is result of factorial user's number
        long factorial = 1;

        // Factorial of user's number
        for (int i = 2; i <= number; i++) {

            factorial = factorial * i;

        }
        System.out.println(factorial);
    }
}

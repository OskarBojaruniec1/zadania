package zadania_petle;

import java.util.Scanner;

public class Task12 {

    public static void main(String[] args) {

        System.out.println("Podaj proszę wyraz: ");

        // User's input
        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine();

        // Convert of user's word to chars
        char[] arr = word.toCharArray();

        // Loop which prints user's word backwards
        for (int i = arr.length - 1; i >= 0; i--) {

            System.out.print(arr[i]);

        }
    }
}



package zadania_petle;

import java.util.Scanner;

public class Task9 {

    public static void main(String[] args) {

        System.out.println("Proszę podaj wyraz: ");

        // User's input
        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine();
        word = word.trim();
        word = word.toLowerCase();

        // Creates a StringBuilder to keep reversed word
        StringBuilder reversedWord = new StringBuilder();

        // Convert of user's word to chars
        char[] resultArray = word.toCharArray();

        // Iteration through user's word to reverse it
        for (int i = resultArray.length - 1; i >= 0; i--){

            // Creating a backward string from chars
            reversedWord.append(resultArray[i]);

        }

        // Checking if user's word is palindrome
        if(word.equals(reversedWord.toString())){

            System.out.println("Twój wyraz jest palindromem!");

        }else{

            System.out.println("Twój wyraz niestety nie jest palindromem");

        }
    }
}
/* Mogliśmy też w tym przypadku użyć metody która posiada StringBuilder reverse(), ale
nie o to chodziło w tym zadaniu. */

